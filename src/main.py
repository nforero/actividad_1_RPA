import algorithm

FINAL_POSITIONS = {
    "M1": (3, 3),
    "M2": (3, 2),
    "M3": (3, 1)
}

ROBOT_POSITION = (2, 2)

MAPA = [
    ["M1", "#", "", "M3"],
    ["", "#", "", ""],
    ["M2", "", "R", ""],
    ["", "F3", "F2", "F1"],
]


def get_inventories_positions(matrix):
    inventories = {}
    for i in range(0, len(matrix)):
        for j in range(0, len(matrix[0])):
            if matrix[i][j].__contains__("M"):
                inventories[matrix[i][j]] = (i, j)
    return inventories


if __name__ == '__main__':
    """Definimos la matriz de 4x4,
    con el estado inicial
    R: representa el robot. Inicialmente está ubicado en la posición [2,2]
    #: representa una pared.
    M1, M2, e M3: representan los tres inventarios que el robot debe mover. Y se encuentran ubicadas en las posiciones
    [0,0], [2,0] y [0,3] respectivamente.
    """
    matrix = [
        ["M1", "#", "", "M3"],
        ["", "#", "", ""],
        ["M2", "", "", ""],
        ["", "", "", ""],
    ]

    INITIAL_POSITIONS = get_inventories_positions(matrix)
    path = algorithm.get_inv_order_path(matrix, ROBOT_POSITION, INITIAL_POSITIONS, FINAL_POSITIONS)

    last_pos = ()
    for i in path:
        if i:
            if last_pos == (i.position[0], i.position[1]):
                content = MAPA[i.position[0]][i.position[1]]
                if content[0] == "M":
                    print(f"R carga el inventario nº {content[1]} en: fila {i.position[0]} columna {i.position[1]}")
                else:
                    print(f"R suelta el inventario nº {content[1]} en: fila {i.position[0]} columna {i.position[1]}")
            else:
                print(f"Mover R a: fila {i.position[0]} columna {i.position[1]}")
            last_pos = (i.position[0], i.position[1])