from Node import Node


def get_distance_between_two_nodes(pos1, pos2):
    """Calculamos la distancia entre dos posiciones dentro del mapa"""
    x = abs(pos2[0] - pos1[0])
    y = abs(pos2[1] - pos1[1])
    return x + y


def get_neighbors(node, matrix, goal):
    neighbors = []
    num_rows = len(matrix)
    num_cols = len(matrix[0])
    for i in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
        row = node.position[0] + i[0]
        col = node.position[1] + i[1]
        if row >= num_rows or row < 0 or col >= num_cols or col < 0:
            continue
        # Evitamos las paredes (#), otros inventarios (Mx), y nodos ya analizados
        if matrix[row][col] not in ["", goal] or (node.parent is not None and (row, col) == node.parent.position):
            continue
        neighbors.append((row, col))

    return neighbors


def a_star(space, start_node, end_node):
    """Algoritmo A*"""
    # Obtenemos los valores de h y f del primer nodo
    start_node.h = get_distance_between_two_nodes(start_node.position, end_node.position)
    start_node.f = start_node.g + start_node.h

    # Obtenemos el nombre del inventario a alcanzar
    goal = space[end_node.position[0]][end_node.position[1]]

    open_list = [start_node]
    closed_list = []
    path = []

    while len(open_list) > 0:

        current_node = open_list[0]
        # Comprobamos si el nodo actual es el nodo objetivo
        if current_node.__eq__(end_node):
            path.append(current_node)
            return path

        childrens = []
        # Obtenemos los siguientes nodos a analizar
        neighbors = get_neighbors(current_node, space, goal)

        if not neighbors:
            # Si no hay mas nodos a analizar se eliminan de la ruta final y se vuelve al nodo de inicio
            current = current_node.parent

            while current.parent is not None:
                path.remove(current)
                current = current.parent
            current_node = current.parent
            path.remove(current)
            open_list.pop(0)
            # En la lista abierta dejamos el nodo de inicio
            open_list.append(current_node)
        else:
            # Creamos los siguientes nodos a analizar
            for child in neighbors:
                childrens.append(Node(current_node, position=(child[0], child[1])))
            # Obtenemos los valores de g, h y f para cada nodo vecino
            for child_node in childrens:
                # Comprobamos que ese nodo no haya sido analizado previamente
                if child_node not in closed_list:
                    child_node.g = current_node.g + 1
                    child_node.h = get_distance_between_two_nodes(child_node.position, end_node.position)
                    child_node.f = child_node.g + child_node.h
            # Si no hay mas nodos para analizar es que no hay ruta hacia ese inventario
            if not childrens:
                return None

            # Ordenamos los nodos hijo por orden de menor f
            childrens = sorted(childrens, key=lambda node: node.f)

            open_list.pop(0)
            # Añadimos el nodo actual a la lista cerrada
            closed_list.append(current_node)
            # En la lista abierta metemos el siguiente nodo
            open_list.append(childrens[0])
            # Añadimos el nodo actual a la ruta final
            path.append(current_node)


def get_inv_order_path(space, robot_position, inv_ini, inv_fin):
    """
    Obtenemos el orden en el que el robot debe mover los inventarios
    """
    inv_order_list = []
    childrens = {}
    # Creamos el Nodo con la posición del robot
    robot_node = Node(position=robot_position)

    # Recorremos la lista de inventarios donde iremos calculando la ruta que debe hacer el robot para alcanzar cada
    # inventario desde su posición actual
    for i in list(inv_fin.keys()):
        inv_node_start = Node(position=inv_ini[i])
        inv_node_end = Node(position=inv_fin[i])
        inv_going = a_star(space, start_node=robot_node, end_node=inv_node_start)
        inv_return = a_star(space, start_node=inv_node_start, end_node=inv_node_end)
        # Si existe ruta de ida y vuelta a ese nodo, se añadirá a la lista de rutas
        if inv_going and inv_going[-1] and inv_return and inv_return[-1]:
            childrens[i] = inv_going + inv_return
    # Ordenamos la lista de rutas de mas corta a mas larga
    childrens_sorted = sorted(childrens.items(), key=lambda x: len(x[1]))
    # Cogemos el Inventarío mas cercano, que será el primero que moverá el robot
    first_inv_key = childrens_sorted[0][0]
    # Lo eliminamos de la lista de inventarios a alcanzar
    inv_fin.pop(first_inv_key)
    # Eliminamos el inventario del mapa
    space[inv_ini[first_inv_key][0]][inv_ini[first_inv_key][1]] = ""
    # Añadimos la ruta
    inv_order_list = inv_order_list + childrens[first_inv_key]
    # Si quedan inventarios por mover, volvemos a aplicar el método de forma recursiva con la posición del robot despues
    # de mover el inventario anterior
    if inv_fin:
        robot_position = childrens[first_inv_key][-1].position
        inv_order_list = inv_order_list + get_inv_order_path(space, robot_position, inv_ini, inv_fin)
    return inv_order_list
